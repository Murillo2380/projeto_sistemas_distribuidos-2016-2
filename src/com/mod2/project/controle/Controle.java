package com.mod2.project.controle;

import com.mod2.project.controle.remoto.cliente.gui.InterfaceLogin;
import com.mod2.project.controle.remoto.gui.EscolhaClienteServidor;
import com.mod2.project.controle.remoto.servidor.ServidorRemoto;

import java.awt.*;
import java.net.UnknownHostException;

/**
 * Classe que decide se este aplicativo irá se comportar como cliente ou servidor.
 */
public class Controle {

    /**
     * Impede que seja feito um new nesta classe
     */
    private Controle(){

    }

    /**
     * Apresenta um diálogo perguntando se o programa deve iniciar como cliente ou servidor
     */
    public static void apresentarEscolhaClienteOuServidor(){
        EscolhaClienteServidor dialog = new EscolhaClienteServidor();
        dialog.pack();
        dialog.setVisible(true);
    }

    /**
     * Inicia este app como um servidor
     * @param porta Porta para o cliente
     */
    public static ServidorRemoto comecarServidor(int porta){
        ServidorRemoto server = null;
        try {
            server = new ServidorRemoto(porta);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        return server;

    }

    /**
     * Inicia este app como um cliente
     * @param porta Porta para o cliente
     */
    public static void comecarCliente(int porta){
        new InterfaceLogin(porta);
    }


    /**
     * Centraliza o canto superior esquerdo de uma janela
     * @param janela Janela a ser centralizada
     */
    public static void centralizarJanela(Window janela){

        Dimension dimensaoJanela = janela.getSize();
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment(); /* informações da janela do computador */
        Point centro = ge.getCenterPoint();
        centro.x = centro.x - dimensaoJanela.width / 2;
        centro.y = centro.y - dimensaoJanela.height / 2;
        janela.setLocation(centro.x, centro.y);

    }


}
