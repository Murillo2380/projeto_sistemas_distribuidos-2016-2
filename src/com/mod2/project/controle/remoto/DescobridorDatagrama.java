package com.mod2.project.controle.remoto;

import java.net.DatagramPacket;

/**
 * Definições de métodos que um gerenciador de datagramas deve conter.
 */
public interface DescobridorDatagrama<T> {

    /**
     * Tamanho máximo do conteúdo do datagrama.
     */
    int DATAGRAMA_TAMANHO_MAXIMO = 1024;

    /**
     * Descobre o tipo de datagrama recebido.
     * @param datagram Datagrama recebido.
     * @return Objeto contendo informações sobre o datagrama recebido.
     */
    T descobrir(DatagramPacket datagram);

    /**
     *
     * @return Dados do remetente o último datagrama descoberto pela função {@link #descobrir(DatagramPacket)}.
     */
    Remoto getRemetente();

}
