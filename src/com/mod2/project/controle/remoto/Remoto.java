package com.mod2.project.controle.remoto;

import com.sun.istack.internal.NotNull;

import java.net.InetAddress;

/**
 * Classe que define atributos de algo remoto.
 */
public class Remoto {

    /**
     * Porta de comunicação
     */
    private int port;

    /**
     * Endereço de comunicação
     */
    private InetAddress address;

    /**
     *
     * @param address Endereço para se comunicar com este objeto remoto.
     * @param port Porta para se comunicar com este objeto remoto
     */
    public Remoto(@NotNull InetAddress address, int port){
        this.address = address;
        this.port = port;
    }

    /**
     *
     * @return Endereço contendo informações para se comunicar com este objeto remoto.
     */
    public InetAddress getIp(){
        return address;
    }

    /**
     *
     * @return Porta que deve ser usada para se comunicar com este elemento remoto.
     */
    public int getPorta(){
        return port;
    }

    @Override
    public boolean equals(Object o){

        if(o instanceof Remoto == false)
            return false;

        Remoto r = (Remoto) o;

        if(getIp().equals(r.getIp()) && getPorta() == r.getPorta())
            return true;
        else
            return false;

    }

}
