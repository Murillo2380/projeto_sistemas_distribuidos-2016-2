package com.mod2.project.controle.remoto.servidor;

import com.mod2.project.controle.remoto.DescobridorDatagrama;
import com.mod2.project.controle.remoto.Remoto;
import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

import java.net.DatagramPacket;

/**
 * Gerenciador de datagrama do servidor, que descobre sobre o que se trada uma mensagem recebida
 */
class DescobridorDatagramaServidor implements DescobridorDatagrama<Integer> {

    /**
     * Retornado quando for recebido 01#
     */
    protected static final int DATAGRAMA_REQUISICAO_LOGIN = 1;

    /**
     * Retornado quando for recebido 02#
     */
    protected static final int DATAGRAMA_REQUISICAO_LOGOUT = 2;

    /**
     * Retornado quando for recebido 03#
     */
    protected static final int DATAGRAMA_REQUISICAO_INICIAR_PARTIDA = 3;

    /**
     * Retornado quando for recebido 04#<msg>
     */
    protected static final int DATAGRAMA_MENSAGEM = 4;

    /**
     * Retornado quando for recebido 05#
     */
    protected static final int DATAGRAMA_REQUISICAO_PUXAR_CARTA = 5;

    /**
     * Retornado quando for recebido 06#
     */
    protected static final int DATAGRAMA_REQUISICAO_PASSAR_VEZ = 6;

    /**
     * Retornado quando for recebido 07#
     */
    protected static final int DATAGRAMA_REQUISICAO_DESISTIR_PARTIDA = 7;

    /**
     * Retornado quando for recebido 08#
     */
    protected static final int DATAGRAMA_KEEP_ALIVE = 8;

    /**
     * Retornado quando o datagrama for desconhecido
     */
    protected static final int DATAGRAMA_DESCONHECIDO = -1;

    /**
     * Retornado quando for identificado um datagrama do tipo {@link #DATAGRAMA_REQUISICAO_LOGIN}
     * e este contém caracteres inválidos (ou # ou ;).
     */
    protected static final int DATAGRAMA_ERRO_NOME_INVALIDO = -2;

    /**
     * Retornado quando o pacote de prefixo 01# tiver um nome maior do que 10 caracteres.
     */
    protected static final int DATAGRAMA_ERRO_NOME_MUITO_GRANDE = -3;

    /**
     * Retornado quando o conteúdo do pacote de mensagens estiver vazio.
     */
    protected static final int DATAGRAMA_ERRO_MENSAGEM_VAZIA = -4;


    /**
     * Contém informações do último objeto remoto que enviou o datagrama.
     */
    private Remoto remoto;

    /**
     *
     * @param datagram Datagrama recebido.
     * @return Inteiro correspondente ao tipo de datagrama recebido.
     * @see #DATAGRAMA_REQUISICAO_LOGIN
     * @see #DATAGRAMA_REQUISICAO_LOGOUT
     * @see #DATAGRAMA_REQUISICAO_INICIAR_PARTIDA
     * @see #DATAGRAMA_MENSAGEM
     * @see #DATAGRAMA_REQUISICAO_PUXAR_CARTA
     * @see #DATAGRAMA_REQUISICAO_PASSAR_VEZ
     * @see #DATAGRAMA_REQUISICAO_DESISTIR_PARTIDA
     * @see #DATAGRAMA_KEEP_ALIVE
     * @see #DATAGRAMA_DESCONHECIDO
     */
    @Override
    public Integer descobrir(@NotNull DatagramPacket datagram) {

        String data = new String(datagram.getData()).trim();
        remoto = new Remoto(datagram.getAddress(), datagram.getPort());

        if(data.startsWith("04#")){

            /* testa se o que vem depois de # é apenas espaços ou se está vazio */
            if(data.substring(data.indexOf("#") + 1).isEmpty() ||
                    data.substring(data.indexOf("#") + 1).matches("( |\n)*"))
                return DATAGRAMA_ERRO_MENSAGEM_VAZIA;

            return DATAGRAMA_MENSAGEM;

        }

        else if(data.startsWith("01#")){
            String nome = data.substring(data.indexOf("#") + 1);

            if(nome.contains("#") || nome.contains(";")){
                return DATAGRAMA_ERRO_NOME_INVALIDO;
            }

            if(nome.length() > 10){
                return DATAGRAMA_ERRO_NOME_MUITO_GRANDE;
            }

            return DATAGRAMA_REQUISICAO_LOGIN;
        }

        else if(data.startsWith("02#")){
            return DATAGRAMA_REQUISICAO_LOGOUT;
        }

        else if(data.startsWith("03#")){
            return DATAGRAMA_REQUISICAO_INICIAR_PARTIDA;
        }

        else if(data.startsWith("05#")){
            return DATAGRAMA_REQUISICAO_PUXAR_CARTA;
        }

        else if(data.startsWith("06#")){
            return DATAGRAMA_REQUISICAO_PASSAR_VEZ;
        }

        else if(data.startsWith("08#")){
            return DATAGRAMA_KEEP_ALIVE;
        }

        else if(data.startsWith("07#")){
            return DATAGRAMA_REQUISICAO_DESISTIR_PARTIDA;
        }

        return DATAGRAMA_DESCONHECIDO;
    }

    /**
     *
     * @return Dados de quem enviou o último datagrama descoberto pela função {@link #descobrir(DatagramPacket)}.
     */
    @Override
    public @Nullable Remoto getRemetente(){
        return remoto;
    }

    /**
     * Avisos que podem ocorrer de acordo com os tipos de datagrama que podem ser reconhecidos pelo servidor.
     */
    interface RetornoRecebimentos {

        /**
         * Chamado sempre que alguém deseja se conectar
         * @param remetente Quem deseja se conectar
         * @param name Nome do jogador que desejou a conexão
         */
        void aoReceberLogin(Remoto remetente, String name);

        /**
         *
         * @param remetente Quem requesitou a desconexão
         */
        void aoReceberLogout(Remoto remetente);

        /**
         * Chamado quando é requesitado uma inicialização da partida de um jogo.
         */
        void aoReceberPedidoIniciarJogo(Remoto remetente);

        /**
         * Chamado quando alguém envia uma mensagem para o servidor.
         * @param remetente Quem enviou a mensagem
         * @param message Mensagem recebida
         */
        void aoReceberMensagem(Remoto remetente, String message);

        /**
         * Chamado quando alguém tenta puxar uma carta
         * @param remetente Quem desejou uma carta
         */
        void aoReceberPedidoPuxarCarta(Remoto remetente);

        /**
         * Chamado quando alguém deseja parar de puxar cartas.
         * @param remetente Quem desejou passar a vez.
         */
        void aoReceberPedidoPassarVez(Remoto remetente);

        /**
         * Chamado quando alguém deseja sair da partida.
         * @param remetente Quem desistiu.
         */
        void aoReceberPedidoDesistirPartida(Remoto remetente);

        /**
         * Chamado quando alguém responde o keep alive.
         * @param remetente Quem respondeu ao keep alive
         */
        void aoReceberKeepAlive(Remoto remetente);

        /**
         * Chamado quando é recebido um pacote com o conteúdo desconhecido.
         * @param remetente Dados do remetente.
         * @param data String do datagrama.
         */
        void aoReceberDatagramaDesconhecido(Remoto remetente, String data);

        /**
         *
         * @param remetente Quem enviou o pacote
         * @param data Dado enviado
         * @param erro Código do erro, podendo ser {@link #DATAGRAMA_ERRO_MENSAGEM_VAZIA}, {@link #DATAGRAMA_ERRO_NOME_INVALIDO}
         *             ou {@link #DATAGRAMA_ERRO_NOME_MUITO_GRANDE}
         */
        void aoIdentificarErro(Remoto remetente, String data, int erro);

    }

}
