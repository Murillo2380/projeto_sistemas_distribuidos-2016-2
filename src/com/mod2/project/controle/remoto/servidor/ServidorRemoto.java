package com.mod2.project.controle.remoto.servidor;

import com.mod2.project.controle.remoto.Remoto;
import com.mod2.project.game.Jogo;
import com.mod2.project.game.components.Carta;
import com.mod2.project.game.components.Jogador;
import com.mod2.project.game.components.lista.ListaJogadores;
import com.mod2.project.util.Espera;
import com.sun.istack.internal.Nullable;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Classe para o servidor deste jogo.
 */
public class ServidorRemoto extends Remoto implements DescobridorDatagramaServidor.RetornoRecebimentos,
        Jogo.PedidosJogo, Espera.Retorno{


    /**
     * Canal de comunicação do servidor
     */
    private ComunicacaoServidor comunicacao;

    private ListaJogadores listaJogadores;
    private ListaJogadores listaEspera;

    /**
     * Indica se o jogador deseja puxar uma carta ou não.
     */
    private volatile boolean jogadorQuerCarta;

    private volatile boolean esperandoJogadorPedirCarta;

    /**
     * Jogador que está na vez de puxar carta ou passar a vez.
     */
    private volatile Jogador jogadorDaVez;

    /**
     * Temporizador usado para esperar o jogador da vez a jogar
     */
    private volatile Timer temporizadorEsperaJogadorDaVez = null;

    /**
     * Temporizador para começar a partida
     */
    private volatile Timer temporizadorInicioPartida = null;


    /**
     * @param portaServidor Porta que este servidor irá usar para comunicação
     */
    public ServidorRemoto(int portaServidor) throws UnknownHostException {
        super(InetAddress.getLocalHost(), portaServidor);

        final DescobridorDatagramaServidor descobridorDatagramaServidor = new DescobridorDatagramaServidor();

        comunicacao = new ComunicacaoServidor(portaServidor, descobridorDatagramaServidor, this);
        comunicacao.start();
        listaJogadores = new ListaJogadores();
        listaEspera = new ListaJogadores();
        jogadorDaVez = null;
        jogadorQuerCarta = false;
        esperandoJogadorPedirCarta = false;

    }

    @Override
    public void aoReceberLogin(Remoto remetente, String name) {

        name = name.substring(name.indexOf("#") + 1); /* corta o início do pacote "01#" */

        Jogador jogador = listaEspera.encontrarJogador(name);

        if(jogador == null)  // se o jogador não foi encontrado na lista de espera, procura na lista de jogadores jogando
            jogador = listaJogadores.encontrarJogador(name);

        if(jogador != null){ /* se o jogador já existir, emite uma mensagem de erro */
            comunicacao.enviarDado(remetente, ConstrutorDatagramaServidor.construir(ConstrutorDatagramaServidor.TIPO_DATAGRAMA_ERRO, "ERRO_NICK_REPETIDO"));
        }
        else { /* se ele não existir, adiciona na lista de espera e avisa todos os jogadores */
            listaEspera.add(new Jogador(remetente.getIp(), remetente.getPorta(), name, this));
            listaEspera.enviarParaTodos(comunicacao, ConstrutorDatagramaServidor.construirListaJogadores(listaEspera, listaJogadores));
            listaJogadores.enviarParaTodos(comunicacao, ConstrutorDatagramaServidor.construirListaJogadores(listaEspera, listaJogadores));
        }
    }

    @Override
    public void aoReceberLogout(Remoto remetente) {

        boolean removidoDaListaJogadoresJogando = false;

        // verifica se o pacote não veio de alguém que não realizou o login
        if(pacoteVeioDeAlguemNaListaDeEspera(remetente) == false && pacoteVeioDeAlguemNaListaDeJogando(remetente) == false)
            return;

        Jogador jogadorQueSaiu = listaEspera.encontrarJogador(remetente.getIp(), remetente.getPorta());
        if(jogadorQueSaiu != null){
            listaEspera.remove(jogadorQueSaiu);
        }

        else{
            jogadorQueSaiu = listaJogadores.encontrarJogador(remetente.getIp(), remetente.getPorta());
            if(jogadorQueSaiu != null){
                listaJogadores.remove(jogadorQueSaiu);
                removidoDaListaJogadoresJogando = true;
            }
        }

        /* avisa a todos sobre a mudança */
        listaEspera.enviarParaTodos(comunicacao, ConstrutorDatagramaServidor.construirListaJogadores(listaEspera, listaJogadores));
        listaJogadores.enviarParaTodos(comunicacao, ConstrutorDatagramaServidor.construirListaJogadores(listaEspera, listaJogadores));

        if(removidoDaListaJogadoresJogando == true){
            mostrarCartas(); // atualiza as cartas
        }

        if(jogadorDaVez != null && jogadorDaVez.equals(jogadorQueSaiu)){
            jogadorQuerCarta = false;
            esperandoJogadorPedirCarta = false;
        }

    }

    @Override
    public void aoReceberPedidoIniciarJogo(Remoto remetente) {

        if(pacoteVeioDeAlguemNaListaDeEspera(remetente) == false)
            return;

        if(jogadorDaVez != null) { // avisa ao remetente que um jogo está em andamento
            comunicacao.enviarDado(remetente,
                    ConstrutorDatagramaServidor.construir(ConstrutorDatagramaServidor.TIPO_DATAGRAMA_MENSAGEM,
                            "Espere o jogo terminar :D"));

            Jogador jogador = encontrarJogador(remetente);
            if(jogador != null)
                jogador.esperarResposta();

            return; // interrompe o restante do código
        }

        if(listaEspera.size() < 2){ // não começa se não houver pelo menos 2 jogadores
            comunicacao.enviarDado(remetente,
                    ConstrutorDatagramaServidor.construir(ConstrutorDatagramaServidor.TIPO_DATAGRAMA_MENSAGEM,
                            "Deve ter pelo menos dois jogadores"));

            comunicacao.enviarDado(remetente, ConstrutorDatagramaServidor.construir(ConstrutorDatagramaServidor.TIPO_DATAGRAMA_ERRO,
                    "ERRO_POUCOS_JOGADORES"));

            Jogador jogador = encontrarJogador(remetente);
            if(jogador != null)
                jogador.esperarResposta();

            return; // interrompe o restante do código
        }


        if(temporizadorInicioPartida != null){ // partida já está para começar
            return;
        }

        listaEspera.enviarParaTodos(comunicacao, ConstrutorDatagramaServidor.construir(ConstrutorDatagramaServidor.TIPO_DATAGRAMA_MENSAGEM,
                "Começando em 5 segundos"));
        listaJogadores.enviarParaTodos(comunicacao, ConstrutorDatagramaServidor.construir(ConstrutorDatagramaServidor.TIPO_DATAGRAMA_MENSAGEM,
                "Começando em 5 segundos"));

        temporizadorInicioPartida = new Timer();
        temporizadorInicioPartida.schedule(new TimerTask() {
            @Override
            public void run() {

                temporizadorInicioPartida = null;
                new Thread(new Runnable(){
                    @Override
                    public void run(){
                        listaJogadores = new ListaJogadores(listaEspera); // clona a lista de espera
                        listaJogadores.marcarNinguemJogou();
                        listaEspera.clear();
                        listaJogadores.enviarParaTodos(comunicacao, ConstrutorDatagramaServidor.construirListaJogadores(listaEspera, listaJogadores));
                        Jogo jogo = new Jogo(ServidorRemoto.this);
                        Jogador vitorioso = jogo.iniciarJogo();

                        if (vitorioso == null) { // empate
                            listaJogadores.enviarParaTodos(comunicacao,
                                    ConstrutorDatagramaServidor.construir(ConstrutorDatagramaServidor.TIPO_DATAGRAMA_MENSAGEM, "Ninguem venceu :("));
                            listaEspera.enviarParaTodos(comunicacao,
                                    ConstrutorDatagramaServidor.construir(ConstrutorDatagramaServidor.TIPO_DATAGRAMA_MENSAGEM, "Ninguem venceu :("));
                        } else {
                            listaJogadores.enviarParaTodos(comunicacao,
                                    ConstrutorDatagramaServidor.construir(ConstrutorDatagramaServidor.TIPO_DATAGRAMA_MENSAGEM,
                                            vitorioso.nick() + " venceu! \\o/"));
                            listaEspera.enviarParaTodos(comunicacao,
                                    ConstrutorDatagramaServidor.construir(ConstrutorDatagramaServidor.TIPO_DATAGRAMA_MENSAGEM,
                                            vitorioso.nick() + " venceu! \\o/"));
                        }

                        jogadorDaVez = null; // ninguém mais está jogando

                        String[] cartas = ConstrutorDatagramaServidor.construirCartas(listaJogadores);
                        String dado = "52#";
                        for(String cartaJogador : cartas){
                            dado = dado + cartaJogador + "#";
                        }
                        listaJogadores.enviarParaTodos(comunicacao, dado);
                        listaEspera.enviarParaTodos(comunicacao, dado);

                        for (Jogador j : listaJogadores) {
                            listaEspera.add(j); // anexa os jogadores na lista de jogadores para a lista de espera
                        }
                        listaJogadores.clear(); // limpa lista de jogadores
                        listaEspera.enviarParaTodos(comunicacao, // atualiza quanto a nova lista de jogadores
                                ConstrutorDatagramaServidor.construirListaJogadores(listaEspera, listaJogadores));
                    }
                }) .start();
            }
        }, 5000); // começa a partida daqui 5 segundos


    }

    @Override
    public void aoReceberMensagem(Remoto remetente, String message) {

        // verifica se o pacote não veio de alguém que não realizou o login
        if(pacoteVeioDeAlguemNaListaDeEspera(remetente) == false && pacoteVeioDeAlguemNaListaDeJogando(remetente) == false)
            return;

        message = message.substring(message.indexOf("#") + 1);

        Jogador jogador = encontrarJogador(remetente);

        if(jogador != null){ /* se o jogador for encontrado, concatena a mensagem ao nome do jogador */
            listaEspera.enviarParaTodos(comunicacao, ConstrutorDatagramaServidor.construir(ConstrutorDatagramaServidor.TIPO_DATAGRAMA_MENSAGEM,
                    jogador.nick() + ": " + message));
            listaJogadores.enviarParaTodos(comunicacao, ConstrutorDatagramaServidor.construir(ConstrutorDatagramaServidor.TIPO_DATAGRAMA_MENSAGEM,
                    jogador.nick() + ": " + message));
        }


    }

    @Override
    public void aoReceberPedidoPuxarCarta(Remoto remetente) {

        // verifica se o pacote não veio de alguém que não realizou o login
        if(pacoteVeioDeAlguemNaListaDeJogando(remetente) == false)
            return;

        // se o pacote veio do jogador que está jogando
        if(jogadorDaVez != null && jogadorDaVez.equals(remetente)) {
            iniciarTemporizadorEsperandoJogadorJogar();
            jogadorQuerCarta = true;
            esperandoJogadorPedirCarta = false;
        }

    }

    @Override
    public void aoReceberPedidoPassarVez(Remoto remetente) {

        if(pacoteVeioDeAlguemNaListaDeJogando(remetente) == false)
            return;

        // se o pacote veio do jogador que está jogando
        if(jogadorDaVez != null && jogadorDaVez.equals(remetente)) {
            pararTemporizadorEsperandoJogadorJogar();
            jogadorQuerCarta = false;
            esperandoJogadorPedirCarta = false;
        }
    }

    @Override
    public void aoReceberPedidoDesistirPartida(Remoto remetente) {

        if(pacoteVeioDeAlguemNaListaDeJogando(remetente) == false)
            return; // ignora o pacote se ele não veio de quem está jogando

        listaEspera.add(encontrarJogador(remetente));
        listaJogadores.remove(remetente);
        listaEspera.enviarParaTodos(comunicacao, ConstrutorDatagramaServidor.construirListaJogadores(listaEspera, listaJogadores));
        listaJogadores.enviarParaTodos(comunicacao, ConstrutorDatagramaServidor.construirListaJogadores(listaEspera, listaJogadores));

        mostrarCartas();

        if(jogadorDaVez.equals(remetente)){
            pararTemporizadorEsperandoJogadorJogar();
            jogadorQuerCarta = false; // passa a vez
            esperandoJogadorPedirCarta = false;
        }

    }

    @Override
    public void aoReceberKeepAlive(Remoto remetente) {

        if(pacoteVeioDeAlguemNaListaDeEspera(remetente) == false && pacoteVeioDeAlguemNaListaDeJogando(remetente) == false)
            return; // ignora pacote caso não tenha vindo de ninguém já logado

        Jogador j = encontrarJogador(remetente);

        if(j != null)
            j.confirmarResposta();

    }

    @Override
    public void aoReceberDatagramaDesconhecido(Remoto remetente, String data) {
        System.out.println("Desconhecido: " + data);
    }

    @Override
    public Jogador proximoJogador() {

        if(listaJogadores.size() <= 1) // se só houver 1 jogador, termina.
            return null;

        jogadorDaVez = listaJogadores.encontrarQuemNaoJogou();

        if(jogadorDaVez == null) // retorna null caso todos os jogadores já tenham jogado.
            return null;

        listaJogadores.enviarParaTodos(comunicacao,
                ConstrutorDatagramaServidor.construir(ConstrutorDatagramaServidor.TIPO_DATAGRAMA_VEZ_JOGADOR,
                String.valueOf(listaJogadores.indexOf(jogadorDaVez)) ));

        listaEspera.enviarParaTodos(comunicacao, // talvez tenha que remover
                ConstrutorDatagramaServidor.construir(ConstrutorDatagramaServidor.TIPO_DATAGRAMA_VEZ_JOGADOR,
                        String.valueOf(listaJogadores.indexOf(jogadorDaVez)) ));

        esperandoJogadorPedirCarta = true; // marca que esta classe deve esperar o jogador pedir uma carta ou não
        return jogadorDaVez;
    }

    @Override
    public boolean pedirCarta() {

        iniciarTemporizadorEsperandoJogadorJogar(); // conta 10 segundos para o jogador jogar

        // espera o jogador responder
        while(esperandoJogadorPedirCarta == true){}

        // marca que esta classe deve esperar o jogador pedir uma carta ou não na próxima vez
        esperandoJogadorPedirCarta = true;
        return jogadorQuerCarta;
    }

    @Override
    public ListaJogadores pedirListaJogadores() {
        return listaJogadores;
    }

    @Override
    public void mostrarCartas(){
        String[] cartas = ConstrutorDatagramaServidor.construirCartas(listaJogadores);
        String dado;

        for(int i = 0; i < listaJogadores.size(); i++){ // passa por todos os jogadores
            dado = "52#";
            for(int j = 0; j < listaJogadores.size(); j++){ // passa por todos os jogadores

                if(j != i){ // se o jogador j não for o mesmo que o jogador i, então troca o início por 0, escondendo a primeira carta
                    dado = dado + String.valueOf(Carta.CARTA_DESCONHECIDA) + cartas[j].substring(cartas[j].indexOf(";")) + "#";
                }
                else{ // manda o pacote normal
                    dado = dado + cartas[j] + "#";
                }

            }

            comunicacao.enviarDado(listaJogadores.get(i), dado); // manda o pacote para o jogador i
            listaJogadores.get(i).esperarResposta(); // aciona temporizador
        }

        dado = "52#";
        for(String carta : cartas){ // esconde a primeira carta de todos os jogadores
            dado = dado + String.valueOf(Carta.CARTA_DESCONHECIDA) + carta.substring(carta.indexOf(";")) + "#";
        }

        listaEspera.enviarParaTodos(comunicacao, dado);
    }

    @Override
    public void aoNaoReceberResposta(Remoto remoto) {

        if(encontrarJogador(remoto) != null) return;

        Jogador jogadorQueSaiu = listaEspera.encontrarJogador(remoto.getIp(), remoto.getPorta());

        if(jogadorQueSaiu != null){
            listaEspera.remove(jogadorQueSaiu);
            listaEspera.enviarParaTodos(comunicacao, ConstrutorDatagramaServidor.construirListaJogadores(listaEspera, listaJogadores));
            listaJogadores.enviarParaTodos(comunicacao, ConstrutorDatagramaServidor.construirListaJogadores(listaEspera, listaJogadores));
        }

        else{
            jogadorQueSaiu = listaJogadores.encontrarJogador(remoto.getIp(), remoto.getPorta());

            if(jogadorQueSaiu != null){
                listaJogadores.remove(jogadorQueSaiu);
                listaEspera.enviarParaTodos(comunicacao, ConstrutorDatagramaServidor.construirListaJogadores(listaEspera, listaJogadores));
                listaJogadores.enviarParaTodos(comunicacao, ConstrutorDatagramaServidor.construirListaJogadores(listaEspera, listaJogadores));
                if(jogadorDaVez.equals(jogadorQueSaiu)){
                    jogadorQuerCarta = false;
                    esperandoJogadorPedirCarta = false;
                }
            }

        }
    }

    @Override
    public void aoIdentificarErro(Remoto remetente, String data, int codigoErro){

        // ignora mensagens vazias
        if(codigoErro == DescobridorDatagramaServidor.DATAGRAMA_ERRO_MENSAGEM_VAZIA)
            return;

        String mensagemErro;

        switch (codigoErro){

            case DescobridorDatagramaServidor.DATAGRAMA_ERRO_NOME_INVALIDO:
                mensagemErro = "ERRO_NICK_INVALIDO";
                break;

            default: // ERRO MENSAGEM MUITO GRANDE
                mensagemErro = "ERRO_NICK_EXTENSO";

        }

        mensagemErro = ConstrutorDatagramaServidor.construir(ConstrutorDatagramaServidor.TIPO_DATAGRAMA_ERRO, mensagemErro);
        comunicacao.enviarDado(remetente, mensagemErro); // avisa sobre o erro

    }

    /**
     * Verifica se o {@code remoto} está presente na lista de espera, verificando seu ip e porta
     * @param remoto Alguém a ser verificado
     * @return {@code true} se está na lista de espera, {@code false} caso contrário
     */
    private boolean pacoteVeioDeAlguemNaListaDeEspera(Remoto remoto){

        if(listaEspera.contains(remoto) == false)
            return false;
        else
            return true;
    }

    /**
     * Verifica se o {@code remoto} está presente na lista de jogadores, verificando seu ip e porta
     * @param remoto Alguém a ser verificado
     * @return {@code true} se está presente na lista de jogadores, {@code false} caso contrário
     */
    private boolean pacoteVeioDeAlguemNaListaDeJogando(Remoto remoto){

        if(listaJogadores.contains(remoto) == false)
            return false;
        else
            return true;
    }

    /**
     *
     * @param remoto Dados do remoto que deverá ser buscado na lista de espera ou de jogadores jogando.
     * @return {@code null} se não for encontrado em nenhuma das listas, caso contrário o objeto jogador será devolvido.
     */
    private @Nullable Jogador encontrarJogador(Remoto remoto){
        Jogador j = listaEspera.encontrarJogador(remoto.getIp(), remoto.getPorta());
        if(j != null)
            return j;

        j = listaJogadores.encontrarJogador(remoto.getIp(), remoto.getPorta());
        return j;
    }

    /**
     * Para o temporizador de espera do jogador da vez caso ele esteja ligado.
     */
    private void pararTemporizadorEsperandoJogadorJogar(){
        if(temporizadorEsperaJogadorDaVez != null){
            temporizadorEsperaJogadorDaVez.cancel();
            temporizadorEsperaJogadorDaVez.purge();
            temporizadorEsperaJogadorDaVez = null;
        }

        temporizadorEsperaJogadorDaVez = null;
    }

    /**
     * Conta 10 segundos para o jogador jogar, avisando a todos sobre este tempo, reiniciando o temporizador
     * anterior caso ele já estivesse contando.
     */
    private void iniciarTemporizadorEsperandoJogadorJogar(){
        pararTemporizadorEsperandoJogadorJogar(); // para se estiver contando
        temporizadorEsperaJogadorDaVez = new Timer();
        temporizadorEsperaJogadorDaVez.schedule(new TimerTask(){
            @Override
            public void run() {
                // Tarefa a se executada caso o jogador da vez não jogue, no caso ele será forçado a passar a evz
                jogadorQuerCarta = false;
                esperandoJogadorPedirCarta = false;
                temporizadorEsperaJogadorDaVez = null;
            }
        }, 10000); // conta 10 segundos

        listaEspera.enviarParaTodos(comunicacao,
                ConstrutorDatagramaServidor.construir(ConstrutorDatagramaServidor.TIPO_DATAGRAMA_TEMPO, "10"));
        listaJogadores.enviarParaTodos(comunicacao,
                ConstrutorDatagramaServidor.construir(ConstrutorDatagramaServidor.TIPO_DATAGRAMA_TEMPO, "10"));

    }

}
