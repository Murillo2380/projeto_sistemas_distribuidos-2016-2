package com.mod2.project.controle.remoto.servidor;

import com.mod2.project.controle.remoto.comunicacao.Comunicacao;
import com.mod2.project.controle.remoto.DescobridorDatagrama;
import com.mod2.project.controle.remoto.Remoto;
import com.mod2.project.controle.remoto.servidor.gui.InterfaceServidor;
import com.mod2.project.util.Espera;
import com.sun.istack.internal.NotNull;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

/**
 * Classe que gerencia as comunicações feitas ao servidor
 */
class ComunicacaoServidor extends Comunicacao<Remoto, String> {

    /**
     * Armazena a classe que deseja receber os avisos definidos nesta interface.
     */
    private DescobridorDatagramaServidor.RetornoRecebimentos retornoRecebimentos;

    private DatagramSocket socket;

    /**
     * Janela que contém um campo para atualizar todas as informações trocadas por este servidor.
     */
    private InterfaceServidor janelaServidor;


    /**
     *
     * @param porta Porta do servidor para esperar por mensagens.
     * @param descobridorDatagramaServidor Gerenciador de datagrama do servidor
     * @param retornoRecebimentos Servidor que deseja receber os avisos definidos em {@link DescobridorDatagramaServidor.RetornoRecebimentos}
     */
    ComunicacaoServidor(int porta, DescobridorDatagramaServidor descobridorDatagramaServidor, @NotNull DescobridorDatagramaServidor.RetornoRecebimentos retornoRecebimentos){
        super(porta, descobridorDatagramaServidor);
        this.retornoRecebimentos = retornoRecebimentos;
        this.janelaServidor = new InterfaceServidor();

        try {
            socket = new DatagramSocket(porta);
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void run(){

        DescobridorDatagramaServidor descobridorDatagramaServidor = (DescobridorDatagramaServidor) getDescobridorDatagrama();
        DatagramPacket packet;
        int pacoteDescoberto;

        while(!isInterrupted()){

            try {
                packet = new DatagramPacket(new byte[DescobridorDatagrama.DATAGRAMA_TAMANHO_MAXIMO], DescobridorDatagrama.DATAGRAMA_TAMANHO_MAXIMO);
                socket.receive(packet);
            } catch (IOException e) {
                //e.printStackTrace();
                interrupt();
                break;
            }

            janelaServidor.atualizarTextoServidor("Recebido de: "
                    + packet.getAddress().getHostAddress() + "/" + String.valueOf(packet.getPort())
                    + System.getProperty("line.separator")
                    + new String(packet.getData()) + System.getProperty("line.separator"));
            System.out.println("Recebido de: "
                    + packet.getAddress().getHostAddress() + "/" + String.valueOf(packet.getPort())
                    + System.getProperty("line.separator")
                    + new String(packet.getData()) + System.getProperty("line.separator"));

            pacoteDescoberto = descobridorDatagramaServidor.descobrir(packet);

            switch (pacoteDescoberto){ /* descobre qual datagrama foi recebido */

                case DescobridorDatagramaServidor.DATAGRAMA_MENSAGEM:
                    retornoRecebimentos.aoReceberMensagem(descobridorDatagramaServidor.getRemetente(), new String(packet.getData()).trim());
                    break;

                case DescobridorDatagramaServidor.DATAGRAMA_REQUISICAO_LOGIN:
                    retornoRecebimentos.aoReceberLogin(descobridorDatagramaServidor.getRemetente(), new String(packet.getData()).trim());
                    break;

                case DescobridorDatagramaServidor.DATAGRAMA_REQUISICAO_LOGOUT:
                    retornoRecebimentos.aoReceberLogout(descobridorDatagramaServidor.getRemetente());
                    break;

                case DescobridorDatagramaServidor.DATAGRAMA_REQUISICAO_PUXAR_CARTA:
                    retornoRecebimentos.aoReceberPedidoPuxarCarta(descobridorDatagramaServidor.getRemetente());
                    break;

                case DescobridorDatagramaServidor.DATAGRAMA_REQUISICAO_PASSAR_VEZ:
                    retornoRecebimentos.aoReceberPedidoPassarVez(descobridorDatagramaServidor.getRemetente());
                    break;

                case DescobridorDatagramaServidor.DATAGRAMA_REQUISICAO_INICIAR_PARTIDA:
                    retornoRecebimentos.aoReceberPedidoIniciarJogo(descobridorDatagramaServidor.getRemetente());

                case DescobridorDatagramaServidor.DATAGRAMA_DESCONHECIDO:
                    retornoRecebimentos.aoReceberDatagramaDesconhecido(descobridorDatagramaServidor.getRemetente(),
                            new String(packet.getData()).trim());
                    break;

                case DescobridorDatagramaServidor.DATAGRAMA_REQUISICAO_DESISTIR_PARTIDA:
                    retornoRecebimentos.aoReceberPedidoDesistirPartida(descobridorDatagramaServidor.getRemetente());
                    break;

                case DescobridorDatagramaServidor.DATAGRAMA_KEEP_ALIVE:
                    retornoRecebimentos.aoReceberKeepAlive(descobridorDatagramaServidor.getRemetente());
                    break;

                case DescobridorDatagramaServidor.DATAGRAMA_ERRO_MENSAGEM_VAZIA: // se for qualquer um desses errors,
                case DescobridorDatagramaServidor.DATAGRAMA_ERRO_NOME_INVALIDO: // avisar que houve erro no pacote
                case DescobridorDatagramaServidor.DATAGRAMA_ERRO_NOME_MUITO_GRANDE:
                    retornoRecebimentos.aoIdentificarErro(descobridorDatagramaServidor.getRemetente(),
                            new String(packet.getData()).trim(), pacoteDescoberto);

            }

        }

        socket.close();

    }


    @Override
    public void enviarDado(Remoto alvo, String dado) {

        byte[] sendData = dado.getBytes();

        if(sendData.length > DescobridorDatagrama.DATAGRAMA_TAMANHO_MAXIMO) { /* se o tamanho do dado exceder o limite, trunca o restante */
            byte[] tmp = new byte[DescobridorDatagrama.DATAGRAMA_TAMANHO_MAXIMO];
            System.arraycopy(tmp, 0, sendData, 0, DescobridorDatagrama.DATAGRAMA_TAMANHO_MAXIMO);
            sendData = tmp;
        }

        try {
            janelaServidor.atualizarTextoServidor("Enviando: " + new String(sendData) + System.getProperty("line.separator"));
            System.out.println("Enviando: " + new String(sendData) + System.getProperty("line.separator"));
            socket.send(new DatagramPacket(sendData, sendData.length, alvo.getIp(), alvo.getPorta()));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
