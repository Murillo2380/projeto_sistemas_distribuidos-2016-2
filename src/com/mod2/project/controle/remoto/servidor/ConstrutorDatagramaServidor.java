package com.mod2.project.controle.remoto.servidor;

import com.mod2.project.game.components.lista.ListaJogadores;
import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

/**
 * Classe que constrói datagramas que deverão ser enviados para os clientes
 */
public class ConstrutorDatagramaServidor {

    /**
     * Constrói um datagrama do tipo mensagem, com o prefixo 54#
     */
    public static final int TIPO_DATAGRAMA_MENSAGEM = 1;

    /**
     * Constrói um datagrama do tipo vez do jogador, com prefixo 55#
     */
    public static final int TIPO_DATAGRAMA_VEZ_JOGADOR = 2;

    /**
     * Constrói um datagrama com prefixo 56#
     */
    public static final int TIPO_DATAGRAMA_TEMPO = 3;

    public static final int TIPO_DATAGRAMA_ERRO = -1;

    /**
     * Construtor privado, não pode dar um "new" nesta classe.
     */
    private ConstrutorDatagramaServidor(){}

    /**
     *
     * @param tipo Tipo de datagrama
     * @param dado Dado do datagrama
     * @return Conteúdo que deverá ser anexado no pacote UDP ou {@code null} em caso de erro
     */
    public static @Nullable String construir(int tipo, @NotNull String dado) {
        String d;

        switch (tipo){

            case TIPO_DATAGRAMA_MENSAGEM:
                d = "54#" + dado;
                break;

            case TIPO_DATAGRAMA_VEZ_JOGADOR:
                d = "55#" + dado;
                break;

            case TIPO_DATAGRAMA_TEMPO:
                d = "56#" + dado;
                break;

            case TIPO_DATAGRAMA_ERRO:
                d = "99#" + dado;
                break;

            default:
                d = null;
        }

        return d;
    }

    /**
     * Gera o pacote 51# com os jogadores na partida e os jogadores em espera
     * @param listaEspera Lista de jogadores em espera
     * @param listaJogadores Lista dos jogadores na partida
     * @return String no formato 51#listaEspera#listaJogadores;
     */
    public static @NotNull String construirListaJogadores(@NotNull ListaJogadores listaEspera, @NotNull ListaJogadores listaJogadores){
        String data = "51#";
        data = data + listaEspera.toString() + "#";

        if(listaJogadores.isEmpty() == false){ /* se tiver jogadores na lista de jogadores na partida */
            data = data + listaJogadores.toString() + ";";
        }

        return data;
    }

    /**
     * Transforma as cartas dos jogadores para uma representação que respeite o protocolo estabelecido para o pacote de
     * prefixo 52#.
     * @param listaJogadores Lista dos jogadores que contém cartas
     * @return Vetor com a string representando as cartas de cada jogador. Cada carta é separada por vírgula. A posição
     *      {@code 0} representa as cartas do primeiro jogador, {@code 1} as cartas do segundo jogador e assim por diante.
     *      Não será adicionado # entre as cartas do jogador, nem mesmo o prefixo 52# sendo que deve ser adicionado manualmente.
     */
    public static @NotNull String[] construirCartas(@NotNull ListaJogadores listaJogadores){

        // cria um vetor de cartas
        String[] cartas = new String[listaJogadores.size()];

        for(int i = 0; i < listaJogadores.size(); i++){
            cartas[i] = listaJogadores.get(i).getCartas().toString();
        }

        return cartas;

    }

}
