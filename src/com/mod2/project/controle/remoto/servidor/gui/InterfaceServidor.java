package com.mod2.project.controle.remoto.servidor.gui;

import com.mod2.project.controle.Controle;
import com.sun.istack.internal.NotNull;

import javax.swing.*;

/**
 * Janela que mostra os dados trocados pelo Servidor
 */
public class InterfaceServidor {
    private JTextArea textAreaLog;
    private JPanel panel1;

    public InterfaceServidor(){

        JFrame frame = new JFrame("Servidor");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        Controle.centralizarJanela(frame);

    }


    /**
     * Atualiza o texto mostrado na tela do servidor, anexando o novo conteúdo no final .
     * @param texto Texto a ser anexado.
     */
    public void atualizarTextoServidor(@NotNull String texto){
        texto = texto.trim();
        if(textAreaLog.getText().length() > 1024) {
            textAreaLog.setText(""); // limpa a area de texto para não ficar muito grande
        }
        textAreaLog.setText(textAreaLog.getText() + texto);
    }

}
