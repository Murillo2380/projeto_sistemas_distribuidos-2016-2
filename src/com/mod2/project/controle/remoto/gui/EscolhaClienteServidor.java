package com.mod2.project.controle.remoto.gui;

import com.mod2.project.controle.Controle;

import javax.swing.*;
import java.awt.event.*;
import java.io.IOException;
import java.net.DatagramSocket;

public class EscolhaClienteServidor extends JDialog {
    private JPanel contentPane;
    private JButton iniciarServidor;
    private JButton iniciarCliente;
    private JTextField textFieldNumPorta;
    private JLabel labelPorta;

    public EscolhaClienteServidor() {

        setTitle("Iniciar blackjack como...");
        setDefaultCloseOperation(DISPOSE_ON_CLOSE); /* termina o programa quando a janela fechar */
        setContentPane(contentPane);
        setModal(true);
        Controle.centralizarJanela(this);
        getRootPane().setDefaultButton(iniciarServidor);

        iniciarServidor.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(textFieldNumPorta.getText().matches("[0-9]+")) {
                    int portaEscolhida = Integer.valueOf(textFieldNumPorta.getText());

                    if(portaDisponivel(portaEscolhida) == true) {
                        Controle.comecarServidor(Integer.valueOf(textFieldNumPorta.getText()));
                        fecharJanela();
                    }
                    else{
                        DialogAviso.mostrarDialog("Porta em uso", "Escolha outra porta");
                    }

                }

                else{
                    DialogAviso.mostrarDialog("Porta invalida", "Insira apenas números");
                }
            }
        });

        iniciarCliente.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(textFieldNumPorta.getText().matches("[0-9]+")) {

                    int portaEscolhida = Integer.valueOf(textFieldNumPorta.getText());

                    if(portaDisponivel(portaEscolhida) == true) { /* veja se a porta já não está ocupada */
                        Controle.comecarCliente(Integer.valueOf(textFieldNumPorta.getText()));
                        fecharJanela();
                    }
                    else{
                        DialogAviso.mostrarDialog("Porta em uso", "Escolha outra porta");
                    }

                }
                else{
                    DialogAviso.mostrarDialog("Porta invalida", "Insira apenas números");
                }
            }
        });

    }

    /**
     * Fecha a janela
     */
    private void fecharJanela() {
        dispose();
    }

    /**
     * Testa se uma porta está disponível ou não
     * @param porta Porta a ser testada
     * @return {@code true} se a porta estiver disponível, {@code false} caso contrário
     */
    private boolean portaDisponivel(int porta){
        DatagramSocket s;
        try{
            s = new DatagramSocket(porta);
            s.close();
            return true;
        }catch (IOException e){
            return false;
        }

    }


}
