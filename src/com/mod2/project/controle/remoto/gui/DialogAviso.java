package com.mod2.project.controle.remoto.gui;

import com.mod2.project.controle.Controle;
import com.sun.istack.internal.NotNull;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Apresenta um aviso na tela com um botão de ok para fechar
 */
public class DialogAviso extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JLabel jLabelAviso;

    private DialogAviso(@NotNull String aviso) {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        jLabelAviso.setText(aviso);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });
    }

    private void onOK() {
// add your code here
        dispose();
    }

    /**
     * Mostra o dialog com o aviso especificado
     * @param titulo Titulo a ser mostrado
     * @param aviso Aviso a ser mostrado
     */
    public static void mostrarDialog(@NotNull String titulo, @NotNull String aviso) {
        DialogAviso dialog = new DialogAviso(aviso);
        dialog.setTitle(titulo);
        Controle.centralizarJanela(dialog);
        dialog.pack();
        dialog.setVisible(true);
    }


}
