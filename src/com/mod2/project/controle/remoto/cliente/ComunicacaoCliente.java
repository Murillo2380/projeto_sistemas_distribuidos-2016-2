package com.mod2.project.controle.remoto.cliente;

import com.mod2.project.controle.remoto.comunicacao.Comunicacao;
import com.mod2.project.controle.remoto.DescobridorDatagrama;
import com.mod2.project.controle.remoto.Remoto;
import com.mod2.project.game.components.Carta;
import com.mod2.project.util.Espera;
import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.ArrayList;

/**
 * Classe que gerencia as comunicações do cliente.
 */
public class ComunicacaoCliente extends Comunicacao<Remoto, String> {

    private DescobridorDatagramaCliente.RetornoRecebimentos retornoRecebimentos;
    private DatagramSocket socket;

    /**
     * @param porta Porta do cliente para esperar o recebimento de mensagens.
     * @param descobridorDatagrama Gerenciador de datagrama para esta classe.
     */
    protected ComunicacaoCliente(int porta, @NotNull DescobridorDatagramaCliente descobridorDatagrama,
                                 @NotNull DescobridorDatagramaCliente.RetornoRecebimentos retornoRecebimentos) {

        super(porta, descobridorDatagrama);
        this.retornoRecebimentos = retornoRecebimentos;
        try {
            socket = new DatagramSocket(porta);
        } catch (SocketException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void run(){

        byte[] buffer = new byte[DescobridorDatagrama.DATAGRAMA_TAMANHO_MAXIMO];
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length);

        DescobridorDatagramaCliente gerenciadorDatagramaCliente = (DescobridorDatagramaCliente) getDescobridorDatagrama();

        while(!isInterrupted()){

            try {
                packet = new DatagramPacket(new byte[DescobridorDatagrama.DATAGRAMA_TAMANHO_MAXIMO], DescobridorDatagrama.DATAGRAMA_TAMANHO_MAXIMO);
                socket.receive(packet); /* espera por um pacote udp */
            } catch (IOException e) {
                //e.printStackTrace();
                interrupt();
                break;
            }

            System.out.println("Recebido: " + new String(packet.getData()).trim());
            switch (gerenciadorDatagramaCliente.descobrir(packet)){ /* descobre qual pacote que chegou */

                case DescobridorDatagramaCliente.DATAGRAMA_MENSAGEM:
                    retornoRecebimentos.mensagemRecebida(gerenciadorDatagramaCliente.getRemetente(), new String(packet.getData()).trim());
                    break;

                case DescobridorDatagramaCliente.DATAGRAMA_LISTA_JOGADORES:
                    retornoRecebimentos.listaDeJogadoresRecebida(gerenciadorDatagramaCliente.getRemetente(), new String(packet.getData()).trim());
                    break;

                case DescobridorDatagramaCliente.DATAGRAMA_INDICE:
                    retornoRecebimentos.indicesRecebidos(gerenciadorDatagramaCliente.getRemetente(),
                            Integer.valueOf(new String(packet.getData()).trim().substring(3 /* pula o 55# */)));
                    break;

                case DescobridorDatagramaCliente.DATAGRAMA_CARTAS:
                    String conteudo = new String(packet.getData()).trim();
                    conteudo = conteudo.substring(3); // pula o prefixo 52#
                    String[] cartas = conteudo.split("#");
                    String[] cartasIndividuais;
                    ArrayList[] listaCartas = new ArrayList[cartas.length];

                    for(int i = 0; i < listaCartas.length; i++){
                        listaCartas[i] = new ArrayList<Carta>();
                        cartasIndividuais = cartas[i].split(";");

                        for(String s : cartasIndividuais){
                            if(s.isEmpty() == false)
                                listaCartas[i].add(new Carta(Integer.valueOf(s)));
                        }

                    }
                    retornoRecebimentos.cartasRecebidas(gerenciadorDatagramaCliente.getRemetente(), listaCartas);
                    break;

                case DescobridorDatagramaCliente.DATAGRAMA_TEMPO:
                    retornoRecebimentos.tempoRecebido(gerenciadorDatagramaCliente.getRemetente(),
                            Integer.valueOf(new String(packet.getData()).trim().substring(3)));
                    break;

                case DescobridorDatagramaCliente.DATAGRAMA_DESCONHECIDO:
                    retornoRecebimentos.datagramaDesconhecidoRecebido(gerenciadorDatagramaCliente.getRemetente(),
                            new String(packet.getData()).trim());
                    break;

                case DescobridorDatagramaCliente.DATAGRAMA_ERRO:
                    retornoRecebimentos.erroRecebido(gerenciadorDatagramaCliente.getRemetente(), new String(packet.getData()).trim());
                break;

            }

        }

        if(socket.isClosed() == false)
            socket.close();


    }

    @Override
    public void enviarDado(Remoto alvo, String dado) {

        byte[] sendData = dado.getBytes();

        if(sendData.length > DescobridorDatagrama.DATAGRAMA_TAMANHO_MAXIMO) { /* se o tamanho do dado exceder o limite, trunca o restante */

            byte[] tmp = new byte[DescobridorDatagrama.DATAGRAMA_TAMANHO_MAXIMO];
            System.arraycopy(tmp, 0, sendData, 0, DescobridorDatagrama.DATAGRAMA_TAMANHO_MAXIMO);
            sendData = tmp;
        }

        try {
            System.out.println("Enviando: " + new String(sendData));
            socket.send(new DatagramPacket(sendData, sendData.length, alvo.getIp(), alvo.getPorta()));
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void interrupt(){
        socket.close();
        super.interrupt();
    }

}
