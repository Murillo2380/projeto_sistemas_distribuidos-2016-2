package com.mod2.project.controle.remoto.cliente;

import com.mod2.project.controle.Controle;
import com.mod2.project.controle.remoto.DescobridorDatagrama;
import com.mod2.project.controle.remoto.Remoto;
import com.mod2.project.controle.remoto.cliente.gui.InterfaceJogo;
import com.mod2.project.controle.remoto.gui.DialogAviso;
import com.mod2.project.game.components.Carta;
import com.mod2.project.game.components.Jogador;
import com.mod2.project.game.components.lista.ListaJogadores;
import com.mod2.project.util.Espera;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Classe que define um cliente remoto
 */
public class ClienteRemoto extends Remoto implements DescobridorDatagramaCliente.RetornoRecebimentos, ActionListener, Espera.Retorno {

    /**
     * Armazena os dados do servidor
     */
    private Remoto servidor;

    /**
     * Objeto que gerencia as comunicações que poderão ser feitas com o cliente
     */
    private ComunicacaoCliente comunicacao;

    private ListaJogadores listaEspera;
    private ListaJogadores listaJogadores;

    /**
     * Nick deste cliente
     */
    private String nick;

    private InterfaceJogo interfaceJogo;

    private Espera espera;

    /**
     * Marca quanto tempo resta para um jogador jogar
     */
    private volatile long tempoRestante = 10;

    /**
     * Temporizador que conta a cada um segundo
     */
    private Timer temporizadorTempoRestante = null;

    /**
     *
     * @param portaCliente Porta que este cliente irá usar para comunicação
     * @param nick Nome deste cliente
     */
    public ClienteRemoto(int portaCliente, String nick) throws UnknownHostException {
        super(InetAddress.getLocalHost(), portaCliente);
        this.nick = nick;
        comunicacao = new ComunicacaoCliente(portaCliente, new DescobridorDatagramaCliente(), this);
        comunicacao.start();

        listaEspera = new ListaJogadores();
        listaJogadores = new ListaJogadores();

        this.interfaceJogo = new InterfaceJogo(this, nick);

        interfaceJogo.getJanela().addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e){
                desconectar(); /* desconecta do servidor e fecha a janela de interface */

                if(interfaceJogo.getJanela().isVisible()) {
                    interfaceJogo.getJanela().setVisible(false);
                    interfaceJogo.getJanela().dispose();
                }

            }
        });

        interfaceJogo.getTextFieldChat().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String mensagem = interfaceJogo.getTextFieldChat().getText();
                mensagem = mensagem.trim();

                if(mensagem.length() > DescobridorDatagrama.DATAGRAMA_TAMANHO_MAXIMO) /* corta a mensagem se ela for muito grande */
                    mensagem = mensagem.substring(0, DescobridorDatagrama.DATAGRAMA_TAMANHO_MAXIMO - 1);

                if(mensagem.isEmpty() == false){ /* envia para o servidor */
                    enviarDado(ConstrutorDatagramaCliente.construir(ConstrutorDatagramaCliente.TIPO_MENSAGEM, mensagem));
                }

                interfaceJogo.getTextFieldChat().setText(""); /* limpa a caixa de texto */
            }
        });

    }

    /**
     * Conecta a um servidor
     * @param address Endereço do servidor
     * @param port Porta do servidor
     */
    public void conectarAoServidor(InetAddress address, int port){
        servidor = new Remoto(address, port);
        espera = new Espera(servidor, 2500, this);
        comunicacao.enviarDado(servidor, ConstrutorDatagramaCliente.construir(ConstrutorDatagramaCliente.TIPO_REQUISICAO_LOGIN, nick));
        espera.esperarResposta();
    }

    /**
     * Envia um dado para o servidor
     * @param data Dado a ser enviado.
     */
    public void enviarDado(String data){
        if(servidor != null) {
            comunicacao.enviarDado(servidor, data);
        }
    }

    /**
     * Termina a conexão com o servidor
     */
    public void desconectar(){
        comunicacao.enviarDado(servidor, ConstrutorDatagramaCliente.construir(ConstrutorDatagramaCliente.TIPO_REQUISICAO_LOGOUT, null));
        comunicacao.interrupt();
        servidor = null;
    }

    @Override
    public void mensagemRecebida(Remoto remetente, String msg) {
        if(servidor.equals(remetente) == false) return; // se o dado não veio do servidor, ignora

        comunicacao.enviarDado(servidor, ConstrutorDatagramaCliente.construir(
                ConstrutorDatagramaCliente.TIPO_KEEP_ALIVE, ""));

        espera.confirmarResposta();
        interfaceJogo.atualizarChat(msg.substring(msg.indexOf("#") + 1));
    }

    @Override
    public void listaDeJogadoresRecebida(Remoto remetente, String mensagem) {

        /* se a lista veio do servidor, aceite */
        if(servidor.equals(remetente) == false) return;

        comunicacao.enviarDado(servidor, ConstrutorDatagramaCliente.construir(
                ConstrutorDatagramaCliente.TIPO_KEEP_ALIVE, ""));

        espera.confirmarResposta(); // confirma que o servidor está online

        String[] mensagemDividida = mensagem.split("#");
        String[] nickJogadoresEsperando = mensagemDividida[1].split(";");
        String[] nickJogadoresJogando = null;

        /* se houver jogadores jogando */
        if(mensagemDividida.length == 3) nickJogadoresJogando = mensagemDividida[2].split(";");

        this.listaEspera.clear();
        for(String nick : nickJogadoresEsperando){
            this.listaEspera.add(new Jogador(null, 0, nick, null));
        }

        this.listaJogadores.clear();
        if(nickJogadoresJogando != null) {
            for (String nick : nickJogadoresJogando) {
                this.listaJogadores.add(new Jogador(null, 0, nick, null));
            }
        }

        else{ // ninguém para jogar, desabilita os botões de passar a vez e pedir carta

            if(temporizadorTempoRestante != null){
                temporizadorTempoRestante.cancel();
                temporizadorTempoRestante = null;
            }

            interfaceJogo.habilitarBotoes(false);
            interfaceJogo.setTextoBotaoComecarDesistir("Começar");
            interfaceJogo.atualizarTituloJogadorDaVez("Ninguém jogando");

        }

        interfaceJogo.atualizarLista(listaEspera, listaJogadores); /* atualiza a interface */

    }

    @Override
    public void cartasRecebidas(Remoto remetente, ArrayList<Carta>[] cartas) {

        if(servidor.equals(remetente) == false) return;

        comunicacao.enviarDado(servidor, ConstrutorDatagramaCliente.construir(
                ConstrutorDatagramaCliente.TIPO_KEEP_ALIVE, ""));

        for(int i = 0; i < listaJogadores.size(); i++){
            listaJogadores.get(i).limparCartas();
            for(Carta c : cartas[i]){ // Entrega as cartas recebidas do servidor ao jogador
                listaJogadores.get(i).adicionarCarta(c);
            }
        }

        interfaceJogo.atualizarCartas(listaJogadores);

    }

    @Override
    public void indicesRecebidos(Remoto remetente, int index) {

        if(servidor.equals(remetente) == false) return;

        comunicacao.enviarDado(servidor, ConstrutorDatagramaCliente.construir(
                ConstrutorDatagramaCliente.TIPO_KEEP_ALIVE, ""));
        espera.confirmarResposta();

        // checa se é a minha vez
        if(nick.equals(listaJogadores.get(index).nick()) == true){ // verifica se é a minha vez
            interfaceJogo.habilitarBotoes(true);
            interfaceJogo.atualizarTituloJogadorDaVez("Sua vez");
        }

        else{ // ainda não é a minha vez
            interfaceJogo.habilitarBotoes(false);
            interfaceJogo.atualizarTituloJogadorDaVez("Vez do " + listaJogadores.get(index).nick());
        }

        interfaceJogo.setTextoBotaoComecarDesistir("Desistir");


    }

    @Override
    public void tempoRecebido(Remoto remetente, int tempo) {
        if(servidor.equals(remetente) == false) return;
        comunicacao.enviarDado(servidor, ConstrutorDatagramaCliente.construir(
                ConstrutorDatagramaCliente.TIPO_KEEP_ALIVE, ""));

        interfaceJogo.atualizarTempoRestante(tempo);
        tempoRestante = tempo;

        if(temporizadorTempoRestante != null)
            temporizadorTempoRestante.cancel();

        temporizadorTempoRestante = new Timer();

        temporizadorTempoRestante.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                interfaceJogo.atualizarTempoRestante(tempoRestante--); //Atualiza a interface sempre que for disparada pelo temporizador
            }
        }, 0, 1000);

    }

    @Override
    public void erroRecebido(Remoto remetente, String error) {
        if(servidor.equals(remetente) == false) return;
        DialogAviso.mostrarDialog("Erro recebido", error.substring(3)); // pula o 99# e mostra o erro
        comunicacao.enviarDado(servidor, ConstrutorDatagramaCliente.construir(
                ConstrutorDatagramaCliente.TIPO_KEEP_ALIVE, ""));
    }

    @Override
    public void datagramaDesconhecidoRecebido(Remoto remetente, String data) {
        if(servidor.equals(remetente) == false) return;

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        switch (e.getActionCommand()) {

            case "Passar vez":
                comunicacao.enviarDado(servidor,
                        ConstrutorDatagramaCliente.construir(ConstrutorDatagramaCliente.TIPO_REQUISICAO_PASSAR_VEZ, ""));
                break;

            case "Puxar carta":
                comunicacao.enviarDado(servidor,
                        ConstrutorDatagramaCliente.construir(ConstrutorDatagramaCliente.TIPO_REQUISICAO_PUXAR_CARTA, ""));
                break;

            case "Desistir":
                comunicacao.enviarDado(servidor,
                        ConstrutorDatagramaCliente.construir(ConstrutorDatagramaCliente.TIPO_REQUESICAO_DESISTIR_PARTIDA, ""));
                break;

            case "Começar":
                comunicacao.enviarDado(servidor,
                        ConstrutorDatagramaCliente.construir(ConstrutorDatagramaCliente.TIPO_REQUESICAO_INICIAR_JOGO, ""));
                break;

            case "Sair":
                interfaceJogo.getJanela().dispatchEvent(new WindowEvent(interfaceJogo.getJanela(), WindowEvent.WINDOW_CLOSING));
                Controle.apresentarEscolhaClienteOuServidor();
                break;

        }

    }

    @Override
    public void aoNaoReceberResposta(Remoto remoto) {
        DialogAviso.mostrarDialog("Falha", "Servidor não está acessível");
        interfaceJogo.getJanela().setVisible(false);
        interfaceJogo.getJanela().dispose();
        comunicacao.interrupt(); // interrompe a comunicação com o servidor, que já não está acessível
        Controle.apresentarEscolhaClienteOuServidor();
    }

}
