package com.mod2.project.controle.remoto.cliente;

import com.sun.istack.internal.Nullable;

/**
 * Classe que contrói pacotes datagrama que serão enviados pelo cliente.
 */
public class ConstrutorDatagramaCliente {

    /**
     * Constrói um datagrama com o prefixo 01#
     */
    public static final int TIPO_REQUISICAO_LOGIN = 1;

    /**
     * Constrói um datagrama com o prefixo 02#
     */
    public static final int TIPO_REQUISICAO_LOGOUT = 2;

    /**
     * Constói um datagrama do tipo mensagem (prefixo 04#)
     */
    public static final int TIPO_MENSAGEM = 3;

    /**
     * Constrói um datagrama com o prefixo 05#
     */
    public static final int TIPO_REQUISICAO_PUXAR_CARTA = 4;

    /**
     * Constrói um datagrama com o prefixo 06#
     */
    public static final int TIPO_REQUISICAO_PASSAR_VEZ = 5;

    /**
     * Constrói um datagrama com o prefixo 03#
     */
    public static final int TIPO_REQUESICAO_INICIAR_JOGO = 6;

    /**
     * Constrói um datagrama com o prefixo 08#
     */
    public static final int TIPO_KEEP_ALIVE = 7;

    /**
     * Constrói um pacote do tipo 07#
     */
    public static final int TIPO_REQUESICAO_DESISTIR_PARTIDA = 8;

    /**
     * Construtor privado para não ser possível fazer "new" nesta classe
     */
    private ConstrutorDatagramaCliente(){}

    /**
     * Constrói o datagrama com o tipo especificado
     * @param tipo Tipo do datagrama
     * @param dado Dados do datagrama
     * @return Conteúdo que deverá ser anexado ao pacote UDP e enviado ou {@code null} em caso de erro.
     */
    public static @Nullable String construir(int tipo, String dado){

        String d;

        switch (tipo){

            case TIPO_REQUISICAO_LOGIN:
                d = "01#" + dado;
                break;

            case TIPO_REQUISICAO_LOGOUT:
                d = "02#";
                break;

            case TIPO_MENSAGEM:
                d = "04#" + dado;
                break;

            case TIPO_REQUISICAO_PUXAR_CARTA:
                d = "05#";
                break;

            case TIPO_REQUISICAO_PASSAR_VEZ:
                d = "06#";
                break;

            case TIPO_REQUESICAO_INICIAR_JOGO:
                d = "03#";
                break;

            case TIPO_REQUESICAO_DESISTIR_PARTIDA:
                d = "07#";
                break;

            case TIPO_KEEP_ALIVE:
                d = "08#";
                break;

            default:
                d = null;

        }

        return d;

    }

}
