package com.mod2.project.controle.remoto.cliente;

import com.mod2.project.controle.remoto.DescobridorDatagrama;
import com.mod2.project.controle.remoto.Remoto;
import com.mod2.project.game.components.Carta;
import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

import java.net.DatagramPacket;
import java.util.ArrayList;

/**
 * Classe que descobre os pacotes que o cliente pode receber.
 */
class DescobridorDatagramaCliente implements DescobridorDatagrama<Integer> {

    /**
     * Retornado quando recebido datagrama com prefixo 51#
     */
    protected static final int DATAGRAMA_LISTA_JOGADORES = 51;

    /**
     * Retornado quando recebido datagrama com prefixo 52#
     */
    protected static final int DATAGRAMA_CARTAS = 52;

    /**
     * Retornado quando recebido datagrama com prefixo 55#
     */
    protected static final int DATAGRAMA_INDICE = 55;

    /**
     * Retornado quando recebido datagrama com prefixo 54#
     */
    protected static final int DATAGRAMA_MENSAGEM = 54;

    /**
     * Retornado quando recebido datagrama com prefixo 56#
     */
    protected static final int DATAGRAMA_TEMPO = 56;

    /**
     * Retornado quando recebido datagrama com prefixo 99#
     */
    protected static final int DATAGRAMA_ERRO = 99;

    /**
     * Retornado quando o datagrama for desconhecido
     */
    protected static final int DATAGRAMA_DESCONHECIDO = -1;

    /**
     *
     */
    protected static final int DATAGRAMA_NUMERO_JOGADORES_INSUFICIENTE = -5;

    /**
     * Retornado quando a mensagem estiver vazia
     */
    protected static final int DATAGRAMA_MENSAGEM_VAZIA = -6;

    /**
     * Contém os dados do remetente do último pacote descoberto
     */
    private Remoto remoto;

    @Override
    public Integer descobrir(@NotNull DatagramPacket datagram) {

        String data = new String(datagram.getData());
        remoto = new Remoto(datagram.getAddress(), datagram.getPort()); /* salva as informações deste pacote */

        if(data.startsWith("54#")){

            if(data.substring(data.indexOf("#") + 1).isEmpty() || data.substring(data.indexOf("#") + 1).matches("( |\n)*"))
                return DATAGRAMA_MENSAGEM_VAZIA;

            return DATAGRAMA_MENSAGEM;

        }

        else if(data.startsWith("51#")){
            return DATAGRAMA_LISTA_JOGADORES;
        }

        // se for 55# seguido de um número
        else if(data.startsWith("55#") && data.substring(3).trim().matches("[0-9]+") == true){
            return DATAGRAMA_INDICE;
        }

        else if(data.startsWith("52#")){
            String conteudo = data.substring(3); // pula 52#
            conteudo = conteudo.trim();

            // verifica se o formato está correto
            if(conteudo.matches("(([0-9]{1,2}|([0-9]{1,2};){1,}[0-9]{1,2})#)+") == true){
                return DATAGRAMA_CARTAS;
            }
            // Tolerância ao ; no final de cada grupo de cartas
            else if(conteudo.matches("(([0-9]{1,2}|([0-9]{1,2};){2,})#)+") == true){
                return DATAGRAMA_CARTAS;
            }

            else
                return DATAGRAMA_DESCONHECIDO;
        }

        else if(data.startsWith("56#") && data.substring(3).trim().matches("[0-9]+") == true){
            return DATAGRAMA_TEMPO;
        }

        else if(data.startsWith("99#")){

            return DATAGRAMA_ERRO;
        }

        return DATAGRAMA_DESCONHECIDO;
    }

    @Override
    public @Nullable
    Remoto getRemetente() {
        return remoto;
    }


    /**
     * Interface que define funções que são chamadas quando um pacote for descoberto.
     */
    public interface RetornoRecebimentos {

        /**
         * Chamado quando uma mensagem for recebida.
         * @param remetente Remetente remoto.
         * @param msg Conteúdo da mensagem.
         */
        void mensagemRecebida(Remoto remetente, String msg);

        /**
         * Chamado quando for recebido um datagrama com as listas de jogadores
         * @param remetente Dados do remetente
         * @param message Datagrama recebido
         */
        void listaDeJogadoresRecebida(Remoto remetente, String message);

        /**
         * Chamada quando for recebido um datagrama contendo as cartas dos jogadores
         * @param remetente Dados do remetente
         * @param cards Carta de cada jogador. A ordem das cartas é a mesma que a ordem do parâmetro {@code players}
         */
        void cartasRecebidas(Remoto remetente, ArrayList<Carta> cards[]);
        
        /**
         * Chamado quando for recebido o datagrama indicando de quem é a vez de jogar.
         * @param remetente Dados do remetente
         * @param index Índice de quem é a vez. O índice respeita a ordem dos jogadores recebido no parâmetro
         */
        void indicesRecebidos(Remoto remetente, int index);

        /**
         * Chamado quando for recebido informações de temporizador.
         * @param remetente Dados do remetente.
         * @param time Tempo em segundos recebido.
         */
        void tempoRecebido(Remoto remetente, int time);

        /**
         * Chamado quando for recebido um erro.
         * @param remetente Dados do remetente.
         * @param error Mensagem de erro recebida
         */
        void erroRecebido(Remoto remetente, String error);

        /**
         * Chamado quando um datagrama de conteúdo desconhecido for recebido.
         * @param remetente Informações do remetente.
         * @param data Conteúdo do pacote recebido.
         */
        void datagramaDesconhecidoRecebido(Remoto remetente, String data);

    }




}
