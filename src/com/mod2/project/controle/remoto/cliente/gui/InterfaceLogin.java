package com.mod2.project.controle.remoto.cliente.gui;

import com.mod2.project.controle.Controle;
import com.mod2.project.controle.remoto.cliente.ClienteRemoto;
import com.mod2.project.controle.remoto.gui.DialogAviso;

import javax.swing.*;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Interface para o login
 */
public class InterfaceLogin {
    private JLabel jLabelNome;
    private JLabel jLabelIPServidor;
    private JLabel jLabelPortaServidor;
    private JButton jButtonLogar;
    public JPanel jPanelRoot;
    private JTextField textFieldIPServidor;
    private JTextField textFieldNome;
    private JTextField textFieldPortaServidor;

    private ClienteRemoto cliente;

    /**
     *
     * @param portaCliente Porta que o cliente irá usar para iniciar uma conexão
     */
    public InterfaceLogin(int portaCliente) {

        JFrame janela = new JFrame("Login");
        janela.setContentPane(jPanelRoot);
        janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        janela.pack();
        janela.setVisible(true);
        Controle.centralizarJanela(janela);

        /* clique do botão logar */
        jButtonLogar.addActionListener(e -> {

            String nome = textFieldNome.getText().trim();
            String ip = textFieldIPServidor.getText().trim();
            String porta = textFieldPortaServidor.getText().trim();

            if (nome.matches("[^#;]{1,10}") == false) { /* se não tiver no máximo 10 caracteres (com excessão de # e ; */
                DialogAviso.mostrarDialog("Nome invalido", "Forneça um nome com no máximo de 10 caracteres sem # ou ;");
            } else if (ip.isEmpty()) {
                DialogAviso.mostrarDialog("IP invalido", "Forneça o ip ou url do servidor");
            } else if (porta.matches("[0-9]+") == false) {
                DialogAviso.mostrarDialog("Porta invalida", "Forneça a porta do servidor usando apenas números");
            } else {
                try { /* verifica se o endereço é válido */
                    InetAddress enderecoIPServidor = InetAddress.getByName(ip);
                    cliente = new ClienteRemoto(portaCliente, nome);
                    cliente.conectarAoServidor(enderecoIPServidor, Integer.parseInt(porta));
                    janela.setVisible(false);
                    janela.dispose();

                } catch (UnknownHostException e1) {
                    //e1.printStackTrace();
                    DialogAviso.mostrarDialog("Erro", "Endereço do servidor não é válido");
                }


            }

        });

    }

}
