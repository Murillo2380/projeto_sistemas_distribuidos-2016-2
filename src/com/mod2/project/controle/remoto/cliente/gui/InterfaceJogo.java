package com.mod2.project.controle.remoto.cliente.gui;

import com.mod2.project.controle.Controle;
import com.mod2.project.game.components.Carta;
import com.mod2.project.game.components.Jogador;
import com.mod2.project.game.components.lista.ListaJogadores;
import com.sun.istack.internal.NotNull;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Classe que contém a interface do jogo
 */
public class InterfaceJogo {
    private JLabel labelListaEspera;
    private JLabel labelListaJogadores;
    private JLabel labelChat;
    private JTextField textFieldChat;
    private JScrollPane scrollPaneListaEspera;
    private JScrollPane scrollPaneListaJogando;
    private JScrollPane scrollPaneChat;
    private JScrollPane scrollPaneCartas;
    private JLabel labelJogo;
    private JButton buttonPassarVez;
    private JButton buttonPuxarCarta;
    private JButton buttonComecarDesistir;
    private JPanel panelRoot;
    private JTextArea textAreaListaEspera;
    private JTextArea textAreaListaJogando;
    private JTextArea textAreaCartas;
    private JTextArea textAreaChat;
    private JButton buttonSair;

    private JFrame janela;

    private String jogadorDaVez = "";

    public InterfaceJogo(@NotNull ActionListener listener, @NotNull String titulo) {

        janela = new JFrame(titulo);
        janela.setContentPane(panelRoot);
        janela.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        janela.pack();
        janela.setVisible(true);
        Controle.centralizarJanela(janela);

        buttonPassarVez.addActionListener(listener);
        buttonComecarDesistir.addActionListener(listener);
        buttonPuxarCarta.addActionListener(listener);
        buttonSair.addActionListener(listener);

        habilitarBotoes(false);

    }


    /**
     * Atualiza a lista de espera
     *
     * @param listaEspera    Lista com os jogadores em espera
     * @param listaJogadores Lista dos jogadores em jogo
     */
    public void atualizarLista(@NotNull ListaJogadores listaEspera, @NotNull ListaJogadores listaJogadores) {

        JTextArea conteudo = (JTextArea) scrollPaneListaEspera.getViewport().getView();
        conteudo.setText(""); /* limpa a lista de espera */

        if (listaEspera.isEmpty() == false) { /* se a lista de espera não estiver vazia */

            for (Jogador jogador : listaEspera) {
                conteudo.setText(conteudo.getText() + System.getProperty("line.separator") + jogador.nick());
            }

            scrollPaneListaEspera.revalidate();
            scrollPaneListaEspera.repaint();

        }

        conteudo = (JTextArea) scrollPaneListaJogando.getViewport().getView();
        conteudo.setText(""); /* limpa a lista de jogadores jogando */

        if (listaJogadores.isEmpty() == false) { /* se a lista de jogadores na partida não estiver fazia */

            for (Jogador jogador : listaJogadores) {
                conteudo.setText(conteudo.getText() + System.getProperty("line.separator") + jogador.nick());
            }
            scrollPaneListaJogando.revalidate();
            scrollPaneListaJogando.repaint();
        }

    }

    /**
     * @param mensagem Mensagem a ser adicionada no chat
     */
    public void atualizarChat(String mensagem) {

        JTextArea chat = (JTextArea) scrollPaneChat.getViewport().getView();
        chat.setText(chat.getText() + System.getProperty("line.separator") + mensagem);

        scrollPaneChat.revalidate();
        scrollPaneChat.repaint();
    }

    /**
     * Atualiza o campo onde deve ser mostrada as cartas dos jogadores
     * @param jogadores Jogadores em jogo para mostrar as cartas
     */
    public void atualizarCartas(@NotNull ListaJogadores jogadores){

        String dado = "";

        for(Jogador j : jogadores){

            dado = dado + j.nick() + ": ";
            for(Carta c : j.getCartas()){ // pega todas as cartas do jogador j
                dado = dado + c.toString() + " ";
            }

            dado = dado + "= " + j.getCartas().somaDasCartas(); // adiciona a soma das cartas no final da linhas
            dado = dado + System.getProperty("line.separator"); // quebra de linha
        }

        JTextArea areaCartas = (JTextArea) scrollPaneCartas.getViewport().getView();
        areaCartas.setText(dado);
        scrollPaneCartas.revalidate();
        scrollPaneCartas.repaint();
    }

    /**
     * Habilita ou desabilita os botões de puxar as cartas ou passar a vez
     *
     * @param habilitado {@code true} para habilitar, {@code false} para desabilitar
     */
    public void habilitarBotoes(boolean habilitado) {

        buttonPuxarCarta.setEnabled(habilitado);
        buttonPassarVez.setEnabled(habilitado);

    }

    /**
     * Atualiza o texto que fica em cima do campo onde aparece as informações das cartas dos jogadores
     * @param dado Dado a ser mostrado
     */
    public void atualizarTituloJogadorDaVez(@NotNull String dado){
        jogadorDaVez = dado;
        labelJogo.setText(dado);
    }

    /**
     * Atualiza a informação referente a quanto tempo resta para um jogador jogar.
     * @param tempoRestante Tempo que deverá ser mostrado ao lado do título do jogo.
     */
    public void atualizarTempoRestante(long tempoRestante){
        labelJogo.setText(jogadorDaVez + " (" + String.valueOf(tempoRestante) + ")");
    }

    /**
     * Atualiza o conteúdo do botão referente as ações de começar e desistir do jogo
     * @param dado Novo texto do botao
     */
    public void setTextoBotaoComecarDesistir(String dado){
        buttonComecarDesistir.setText(dado);
    }

    /**
     * @return Objeto janela associada a esta interface.
     */
    public @NotNull JFrame getJanela() {
        return janela;
    }

    /**
     *
     * @return Caixa de texto para digitar mensagens no chat.
     */
    public @NotNull JTextField getTextFieldChat(){
        return textFieldChat;
    }


}
