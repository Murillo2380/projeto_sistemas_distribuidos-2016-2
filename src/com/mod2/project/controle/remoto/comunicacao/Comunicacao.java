package com.mod2.project.controle.remoto.comunicacao;

import com.mod2.project.controle.remoto.DescobridorDatagrama;
import com.sun.istack.internal.NotNull;

/**
 * Classe abstrata que gerencia uma comunicação UDP.
 */
public abstract class Comunicacao<T, D> extends Thread{

    /**
     * Contém o gerenciador de datagrama desta classe.
     */
    private DescobridorDatagrama descobridorDatagrama;

    /**
     * Armazena a fonte de comunicação, indicando como os dados devem ser recebidos
     */
    private int porta;
    /**
     *
     * @param port Porta de comunicação
     * @param descobridorDatagrama Gerenciador de datagrama para esta classe
     */
    protected Comunicacao(int port, @NotNull DescobridorDatagrama descobridorDatagrama){
        this.porta = port;
        this.descobridorDatagrama = descobridorDatagrama;
    }

    /**
     *
     * @return Gerenciador de datagrama associado a esta classe
     */
    protected @NotNull DescobridorDatagrama getDescobridorDatagrama(){
        return descobridorDatagrama;
    }

    /**
     *
     * @return Porta de comunicação atribuída a esta classe através do construtor.
     */
    protected int getPortaComunicacao(){
        return porta;
    }

    /**
     * Envia um dado para um determinado alvo.
     * @param alvo Quem deve receber o dado.
     * @param dado Dado a ser enviado.
     */
    public abstract void enviarDado(T alvo, D dado);

}
