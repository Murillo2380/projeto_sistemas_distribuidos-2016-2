package com.mod2.project.util;

import com.mod2.project.controle.remoto.Remoto;
import com.sun.istack.internal.NotNull;

import java.util.Timer;
import java.util.TimerTask;


/**
 * <p>Classe que auxilia quando há interesse em saber se algo remoto respondeu ou não, usando um temporizador para
 * esperar pela resposta.</p>
 * <p>Para começar a espera pela resposta, use {@link #esperarResposta()}.</p>
 * <p>Para confirmar a resposta, use {@link #confirmarResposta()}.</p>
 * <p>Caso a respsota não tenha sido confirmada, a função {@link Retorno#aoNaoReceberResposta(Remoto)} será chamada.</p>
 */
public class Espera {

    /**
     * Quem deverá ser chamado caso não haja uma resposta após um determinado tempo
     */
    private Retorno retorno;

    private final Remoto remoto;
    private final long tempoEspera;
    private volatile java.util.Timer temporizador;

    /**
     *
     * @param remoto Algo remoto que confirmará a resposta ou não.
     * @param tempoEspera Tempo em que o {@code remoto} tem para responder
     * @param retorno Quem deverá ser chamado caso não haja uma resposta.
     *
     */
    public Espera(@NotNull Remoto remoto, long tempoEspera, @NotNull Retorno retorno){
        this.remoto = remoto;
        this.tempoEspera = tempoEspera;
        this.retorno = retorno;
    }

    /**
     * Inicia o temporizador para esperar pela resposta do remoto. Caso um temporizador já esteja contando,
     * ele será reiniciado. Após o tempo de espera, a função {@link Retorno#aoNaoReceberResposta(Remoto)} será
     * chamada
     */
    public void esperarResposta(){

        if (temporizador != null) { // cancela um temporizador se ele já tiver sido criado antes
            temporizador.cancel();
            temporizador.purge();
        }

        temporizador = new Timer();
        temporizador.schedule(new TimerTask() {
            @Override
            public void run() {
                retorno.aoNaoReceberResposta(remoto); // agenda a chamada desta função após um tempo de espera
            }
        }, tempoEspera);


    }

    /**
     * Confirma que o {@code remoto} respondeu, previnindo a chamada da função {@link Retorno#aoNaoReceberResposta(Remoto)}}
     */
    public void confirmarResposta(){

        if (temporizador != null) {
            temporizador.cancel();
            temporizador.purge();
            temporizador = null;
        }

    }

    /**
     * Interface que possui a função que será chamada após uma falha no recebimento de resposta.
     * @see Espera
     */
    public interface Retorno {

        /**
         * Chamada após um um objeto remoto não responder.
         * @param remoto Dados do destinatário que não respondeu.
         * @see Espera
         */
        void aoNaoReceberResposta(Remoto remoto);
    }

}
