package com.mod2.project.game;

import com.mod2.project.game.components.Carta;
import com.mod2.project.game.components.Jogador;
import com.mod2.project.game.components.lista.ListaJogadores;
import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Random;
import java.util.Vector;

/**
 * Indefinida por enquanto.
 */
public class Jogo {

    private PedidosJogo pedidosJogo;

    /**
     * Lista dos jogadores em jogo
     */
    private ListaJogadores jogadores;

    private Random gerador;

    /**
     *
     * @param pedidosJogo Classe que implementa esta interface, de modo que o jogo possa fazer pedidos a esta classe
     */
    public Jogo(@NotNull PedidosJogo pedidosJogo){
        this.pedidosJogo = pedidosJogo;
        gerador = new Random(System.currentTimeMillis());
    }

    /**
     * Inicia o jogo.
     * @return Jogador que ganhou ou {@code null} caso ninguém tenha ganhado
     */
    public @Nullable Jogador iniciarJogo(){

        Jogador jogadorDaVez;
        boolean jogadorGanhou = false;

        ListaJogadores jogadores = pedidosJogo.pedirListaJogadores();
        for(Jogador j : jogadores){ // passa por cada jogador e sorteia duas cartas para ele
            j.adicionarCarta(new Carta(escolherCartaDoMonte()));
            j.adicionarCarta(new Carta(escolherCartaDoMonte()));
        }

        pedidosJogo.mostrarCartas();

        while((jogadorDaVez = pedidosJogo.proximoJogador()) != null){

            while(pedidosJogo.pedirCarta() == true){ // enquanto o jogador quiser carta
                jogadorDaVez.adicionarCarta(new Carta(escolherCartaDoMonte()));
                pedidosJogo.mostrarCartas();

                if(jogadorDaVez.getCartas().somaDasCartas() == 21) { // se o jogador conseguir 21 pontos
                    jogadorGanhou = true;
                    break;
                }
                // interrompe o processo de pedir cartas visto que o jogador estourou o limite de 21 pontos
                else if(jogadorDaVez.getCartas().somaDasCartas() > 21)
                    break;
            }

            if(jogadorGanhou == true)
                return jogadorDaVez;
            else
                jogadorDaVez.setJaJogou(true); // marca que já jogou

        }

        jogadores = pedidosJogo.pedirListaJogadores();
        jogadores = new ListaJogadores(jogadores); // cria uma cópia para não alterar a lista original

        jogadores.sort(new Comparator<Jogador>() {
            @Override
            public int compare(Jogador o1, Jogador o2) {
                return o2.getCartas().somaDasCartas() - o1.getCartas().somaDasCartas(); /* utiliza a pontuação das cartas como critério na ordenação */
            }
        });

        Iterator<Jogador> iterator = jogadores.iterator();
        Jogador j;

        while(iterator.hasNext() == true){ // passa por todos os jogadores na lista de clientes jogando
            j = iterator.next();
            if(j.getCartas().somaDasCartas() < 21){ // se ele tiver menos de 21 pontos
                if(iterator.hasNext() == true){ // se houver um proximo verifica a pontuação do outro jogador

                    if(iterator.next().getCartas().somaDasCartas() == j.getCartas().somaDasCartas()) return null; // empate
                    else return j; /* jogador j ganhou */

                }
                else return j;
            }
        }

        return jogadorDaVez; // sempre null, verificar função novamente
    }

    /**
     * Escolhe uma carta elatória do monte.
     * @return Número da nova carta entre {@code 1} (inclusive) e {@code 13} (inclusive) de acordo com as constantes definidas na classe
     * {@link com.mod2.project.game.components.Carta}
     * @see com.mod2.project.game.components.Carta#CARTA_AS
     * @see com.mod2.project.game.components.Carta#CARTA_DOIS
     * @see com.mod2.project.game.components.Carta#CARTA_TRES
     * @see com.mod2.project.game.components.Carta#CARTA_QUATRO
     * @see com.mod2.project.game.components.Carta#CARTA_CINCO
     * @see com.mod2.project.game.components.Carta#CARTA_SEIS
     * @see com.mod2.project.game.components.Carta#CARTA_SETE
     * @see com.mod2.project.game.components.Carta#CARTA_OITO
     * @see com.mod2.project.game.components.Carta#CARTA_NOVE
     * @see com.mod2.project.game.components.Carta#CARTA_DEZ
     * @see com.mod2.project.game.components.Carta#CARTA_DAMA
     * @see com.mod2.project.game.components.Carta#CARTA_VALETE
     * @see com.mod2.project.game.components.Carta#CARTA_REIS
     */
    private int escolherCartaDoMonte(){

        return gerador.nextInt(13) + 1; /* gera um número entre 0 e 12 e soma 1 para estar entre 1 e 13 */
    }



    /**
     * Interface que define pedidos que o jogo fará aos jogadores.
     */
    public interface PedidosJogo{

        /**
         * Chamado quando o jogo pede o próximo jogador da vez
         * @return Próximo jogador ou {@code null} caso não haja um próximo jogador
         */
        @Nullable Jogador proximoJogador();

        /**
         * Chamado quando o jogo deseja que o jogador descida se ele quer pedir uma nova carta ou não
         * @return {@code true} Se o jogador quer uma nova carta, {@code false} caso contrário.
         */
        boolean pedirCarta();

        /**
         * Chamado quando o jogo deseja receber a lista de todos os jogadores na partida. A lista de jogadores
         * que será retornada por esta função pode passar por um processo de ordenação, logo a lista retornada
         * por esta função pode ter a ordem dos elementos alterada. Quando esta função é chamada, a partida está iniciando ou
         * está prestes a terminar
         * @return Lista de jogadores atualmente jogando.
         */
        ListaJogadores pedirListaJogadores();

        /**
         * Chamada quando o jogo pede que as cartas dos jogadores sejam mostradas
         */
        void mostrarCartas();

    }

}
