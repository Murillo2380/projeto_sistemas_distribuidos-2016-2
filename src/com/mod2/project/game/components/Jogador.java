package com.mod2.project.game.components;

import com.mod2.project.controle.remoto.Remoto;
import com.mod2.project.game.components.lista.ListaCartas;
import com.mod2.project.util.Espera;
import com.sun.istack.internal.NotNull;

import java.net.InetAddress;

/**
 * Classe com atributos de um jogador
 */
public class Jogador extends Remoto {

    /**
     * Nome do jogador
     */
    private String nick;

    /**
     * {@code true} se este jogador já jogou, {@code false} caso contrário
     */
    private volatile boolean jaJogou = false;

    private final Espera espera;

    private ListaCartas cartas = new ListaCartas();

    /**
     *
     * @param ip Ip deste jogador
     * @param porta Porta deste jogador
     * @param nick Nome do jogador
     * @param retorno Classe que deve ser chamada caso o jogador não responda a um envio de dado.
     */
    public Jogador(InetAddress ip, int porta, String nick, @NotNull Espera.Retorno retorno){
        super(ip, porta);
        this.nick = nick;
        espera = new Espera(this, 5000, retorno);
    }

    /**
     * Aguarda uma resposta desse jogador, caso ele não responda, o retorno passado pelo
     * construtor será chamado.
     * @see Jogador
     * @see #confirmarResposta()
     */
    public void esperarResposta(){
        synchronized (espera) {
            espera.esperarResposta();
        }
    }

    /**
     * Confirma uma resposta deste jogador.
     * @see #esperarResposta()
     */
    public void confirmarResposta(){
        synchronized (espera){
            espera.confirmarResposta();
        }
    }

    /**
     *
     * @return Nome do jogador
     */
    public String nick(){
        return nick;
    }

    /**
     * Adiciona um carta para este jogador
     * @param carta Carta a ser adicionada
     */
    public void adicionarCarta(Carta carta){
        cartas.add(carta);
    }

    /**
     * Limpa a lista de cartas desse jogador.
     */
    public void limparCartas(){
        cartas.clear();
    }

    /**
     *
     * @param jaJogou {@code true} caso queira marcar que este jogador já realizou as suas jogadas, {@code false} caso contrário
     */
    public void setJaJogou(boolean jaJogou){
        this.jaJogou = jaJogou;
    }

    /**
     *
     * @return {@code true } se este jogador já jogou, {@code false} caso contrário
     */
    public boolean jaJogou(){
        return jaJogou;
    }

    public ListaCartas getCartas(){
        return cartas;
    }


}
