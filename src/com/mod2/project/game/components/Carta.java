package com.mod2.project.game.components;

/**
 * Classe que indica um tipo de carta.
 */
public class Carta {

    public static final int CARTA_DESCONHECIDA = 0;
    public static final int CARTA_AS = 1;
    public static final int CARTA_DOIS = 2;
    public static final int CARTA_TRES= 3;
    public static final int CARTA_QUATRO = 4;
    public static final int CARTA_CINCO = 5;
    public static final int CARTA_SEIS = 6;
    public static final int CARTA_SETE = 7;
    public static final int CARTA_OITO = 8;
    public static final int CARTA_NOVE = 9;
    public static final int CARTA_DEZ = 10;
    public static final int CARTA_VALETE = 11;
    public static final int CARTA_DAMA = 12;
    public static final int CARTA_REIS = 13;

    /**
     * Armazena o tipo da carta
     */
    private int type;

    public Carta(int type){
        this.type = type;
    }

    /**
     *
     * @return Número da carta, podendo ser
     * {@link #CARTA_DESCONHECIDA}
     * {@link #CARTA_AS}
     * {@link #CARTA_DOIS}
     * {@link #CARTA_TRES}
     * {@link #CARTA_QUATRO}
     * {@link #CARTA_CINCO}
     * {@link #CARTA_SEIS}
     * {@link #CARTA_SETE}
     * {@link #CARTA_OITO}
     * {@link #CARTA_NOVE}
     * {@link #CARTA_DEZ}
     * {@link #CARTA_VALETE}
     * {@link #CARTA_DAMA}
     * {@link #CARTA_REIS}
     */
    public int getType(){
        return type;
    }

    /**
     *
     * @return Ponto da carta, podendo ser: {@code 11} para AS, {@code 10} para Valete, Dama ou Reis ou o valor numérico
     *      da carta.
     */
    public int getPonto(){
        if(type >= CARTA_DEZ) return 10;
        else return type;
    }

    /**
     *
     * @return Representação textual da carta
     */
    @Override
    public String toString(){

        switch (type){
            case CARTA_DESCONHECIDA:
                return "*";

            case CARTA_AS:
                return "A";

            case CARTA_VALETE:
                return "J";

            case CARTA_DAMA:
                return "Q";

            case CARTA_REIS:
                return "K";

            default: /* Carta numérica */
                return String.valueOf(type);

        }

    }

}
