package com.mod2.project.game.components.lista;

import com.mod2.project.game.components.Carta;

import java.util.Vector;

/**
 * Armazena uma lista de cartas
 */
public class ListaCartas extends Vector<Carta> {

    /**
     *
     * @return Soma das cartas de acordo com as regras do 21
     */
    public int somaDasCartas(){

        int total = 0;

        for(Carta c : this) { // itera sobre todas as cartas na lista de cartas
                total = total + c.getPonto();
        }

        return total;

    }


    /**
     *
     * @return Número das cartas separadas por ;
     */
    @Override
    public String toString(){

        String dado = "";

        if(isEmpty())
            return ""; /* Se não houver ninguém na lista retorna uma string vazia */

        for(Carta carta : this){ /* Para cada carta, coloque o nome da carta com um ;*/
            dado = dado + String.valueOf(carta.getType()) + ";";
        }

        return dado.substring(0, dado.length() - 1); /* remove o último ";" */

    }

}
