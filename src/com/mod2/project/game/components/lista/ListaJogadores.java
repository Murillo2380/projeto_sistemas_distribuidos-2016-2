package com.mod2.project.game.components.lista;

import com.mod2.project.controle.remoto.comunicacao.Comunicacao;
import com.mod2.project.controle.remoto.Remoto;
import com.mod2.project.game.components.Jogador;
import com.mod2.project.util.Espera;
import com.sun.istack.internal.Nullable;

import java.net.InetAddress;
import java.util.Vector;

/**
 * Lista de jogadores
 */
public class ListaJogadores extends Vector<Jogador> {

    public ListaJogadores(){
        super();
    }

    public ListaJogadores(Vector<Jogador> jogadores){ // construtor para clonar o arraylist recebido
        super(jogadores);
    }

    /**
     * Envia uma string para todos os jogadores utilizando o canal de comunicação.
     * @param communication Comunicação usada para enviar dados
     * @param data Dado a ser enviado
     */
    public void enviarParaTodos(Comunicacao<Remoto, String> communication, String data){

        for(Jogador jogador : this) { /* para todos os jogadores desta lista */
            communication.enviarDado(jogador, data);
            jogador.esperarResposta();
        }

    }

    /**
     * Procura um jogador na lista com as informações especificadas
     * @param ip Ip do jogador a ser encontrado
     * @param porta Porta do jogador a ser encontrado
     * @return Jogador com ip e porta especificados ou {@code null} caso contrário.
     */
    public @Nullable Jogador encontrarJogador(InetAddress ip, int porta){

        for(Jogador jogador : this){

            if(jogador.getIp().equals(ip) && jogador.getPorta() == porta)
                return jogador;

        }

        return null;
    }

    /**
     * Passa por todos os elementos da lista e chama a função {@link Jogador#setJaJogou(boolean)} passando como parâmetro
     * {@code false}. Também chama a função {@link Jogador#limparCartas()} para resetar todas as cartas do jogador
     */
    public void marcarNinguemJogou(){

        for(Jogador jogador : this) {
            jogador.setJaJogou(false);
            jogador.limparCartas();
        }

    }

    /**
     * Chama a função {@link Jogador#jaJogou()} de cada jogador da lista e verifica o valor booleano retornado.
     * @return {@code null} caso todos os jogadores desta lista já tenham jogado, caso contrário retorna
     * um jogador da lista que ainda não jogou
     */
    public @Nullable Jogador encontrarQuemNaoJogou(){

        for(Jogador jogador : this){
            if(jogador.jaJogou() == false)
                return jogador;
        }

        return null;
    }

    /**
     * Procura um jogador na lista com as informações especificadas
     * @param nick Nick do jogador a ser encontrado.
     * @return Jogador que contém todas as informações especificadas ou {@code null} caso contrário.
     */
    public @Nullable Jogador encontrarJogador(String nick){

        for(Jogador jogador : this){
            if(jogador.nick().equals(nick))
                return jogador;
        }

        return null;
    }

    /**
     *
     * @return Nome de todos os jogadores, separados por ;
     */
    @Override
    public String toString(){

        String data = "";

        if(isEmpty()) /* se não houver jogadores na lista */
            return "";

        for(Jogador jogador : this){ /* para todos os jogadores nesta lista */
            data = data + jogador.nick() + ";"; /* concatena o nome do jogador com  ";" */
        }

        return data.substring(0, data.length() - 1); /* remove o último ponto e vírgula */

    }

}
